FROM python:3.11

WORKDIR /app

COPY requirements.txt config.yaml ./

RUN pip install -r requirements.txt

COPY matrix_reminder_bot/ ./matrix_reminder_bot/

CMD ["python", "matrix_reminder_bot/main.py"]
