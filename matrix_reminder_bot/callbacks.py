import logging

from nio import (
    AsyncClient,
    InviteMemberEvent,
    JoinError,
    MatrixRoom,
    RoomMessageText,
)

from matrix_reminder_bot.config import CONFIG
from matrix_reminder_bot.functions import send_text_to_room
from matrix_reminder_bot.storage import Storage

logger = logging.getLogger(__name__)


class Callbacks(object):
    """Callback methods that fire on certain matrix events

    Args:
        client: nio client used to interact with matrix
        store: Bot storage
    """

    def __init__(self, client: AsyncClient):
        self.client = client
        #self.store = store

    async def message(self, room: MatrixRoom, event: RoomMessageText):
        """Callback for when a message event is received"""
        # Ignore broken events
        if not event.body:
            return

        if event.decrypted:
            encrypted_symbol = "🛡 "
        else:
            encrypted_symbol = "⚠️ "
        print(
            f"{room.display_name} |{encrypted_symbol}| {room.user_name(event.sender)}: {event.body}"
        )

        logger.debug( f"{room.display_name} |decrypted = {event.decrypted}| {room.user_name(event.sender)}: {event.body}")


    async def invite(self, room: MatrixRoom, event: InviteMemberEvent):
        """Callback for when an invite is received. Join the room specified in the invite"""
        logger.debug(f"Got invite to {room.room_id} from {event.sender}.")

        # Attempt to join 3 times before giving up
        for attempt in range(3):
            result = await self.client.join(room.room_id)
            if type(result) == JoinError:
                logger.error(
                    f"Error joining room {room.room_id} (attempt %d): %s",
                    attempt,
                    result.message,
                )
            else:
                logger.info(f"Joined {room.room_id}")
                break
