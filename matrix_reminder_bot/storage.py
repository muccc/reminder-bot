import logging
import os
import icalendar
from nio import AsyncClient

from matrix_reminder_bot.config import CONFIG
from matrix_reminder_bot.reminder import REMINDERS, Reminder
from matrix_reminder_bot.errors import ConfigError

latest_migration_version = 3

logger = logging.getLogger(__name__)


class Storage(object):
    def __init__(self, client: AsyncClient):
        """Load reminders

        Loads reminders from calendar or config file

        Args:
            client: The matrix client
        """
        self.client = client

        # Load reminders from the db
        REMINDERS.update(self._load_reminders())

    def _load_reminders(self):
        """Load reminders from calendar of config file

        Returns:
            A dictionary from reminder_name to Reminder object
        """

        self.reminders = {}
        if CONFIG.calendar_path:
            self._get_events_from_calendar()
        else:
            self._get_events_from_config()

        return self.reminders

    def _get_events_from_calendar(self):
        """
        Load events from calendar
        """
        if not os.path.isfile(CONFIG.calendar_path):
            raise ConfigError(f"Calendar file '{CONFIG.calendar_path}' does not exist")

        with open(CONFIG.calendar_path) as f:
            self.calendar = icalendar.Calendar.from_ical(f.read())

        for event in self.calendar.walk('vevent'):
            # Extract reminder data
            event_text = event.get("SUMMARY")
            logger.info("Loaded event with summary: %s", event_text)
            event_text = event_text.upper().replace(" ", "_")

            # check if a message for this event has been confifgured. if not use the events' name as message
            if event_text in CONFIG.event_messages.keys():
                reminder_text = CONFIG.event_messages[event_text]
            else:
                reminder_text = event.get("SUMMARY")

            logger.info("reminder_text: %s", reminder_text)

            start_time = event.get("DTSTART").dt
            logger.info("Loaded event with start_time: %s", start_time)

            # check if an rrule exists for this event.
            # if there is a rrule convert the rrule and start_time to an cron_tab
            if event.get("RRULE"):
                rrule = event.get("RRULE")
                logger.info("Loaded event with rrule: %s", rrule)
                cron_tab = self._get_cron_tab_from_rrule(rrule, start_time)
                logger.info("Store reminder with cron_tab: %s", cron_tab)
            
                # Create and record the reminder
                self.reminders[(event_text)] = Reminder(
                    client=self.client,
                    reminder_text=reminder_text,
                    cron_tab=cron_tab,
                )
            # store one time event with start time
            else:
                self.reminders[(event_text)] = Reminder(
                    client=self.client,
                    reminder_text=reminder_text,
                    start_time=start_time,
                )

    def _get_events_from_config(self):
        """
        Load events from config file
        """
        for event in CONFIG.events:
            # Extract reminder data
            reminder_name = event["name"].upper().replace(" ", "_")
            reminder_text = event["event_message"]
            logger.info("Loaded event with name: %s", reminder_name)
            logger.info("reminder_text: %s", reminder_text)

            if "recur" in event.keys():
                logger.info("Loaded event with recur rule: %s", event["recur"])
                rule = event["recur"].split(" ")
                # conver recur rules  day field as '2th#thu' to '2th thu' as CronTrigger likes them 
                rule[2] = rule[2].replace('#', ' ')
                several_dates = 0
                for entry in rule:
                    if "," in entry:
                        several_dates += 1
                
                logger.info("several_dates: %s", several_dates)
                if several_dates == 0:
                    logger.info("Create event with cron_tab: %s", rule)
                    self.reminders[reminder_name] = Reminder(
                        client=self.client,
                        reminder_text=reminder_text,
                        cron_tab=rule,
                    )
                elif several_dates == 1:
                    idx = 0
                    for i in range(len(rule)):
                        if "," in rule[i]:
                            idx = i
                            break
                    multi_entry = rule[idx].split(",")

                    for i in range(len(multi_entry)):
                        new_rule = ["", "", "", "", ""]
                        for j in range(len(rule)):
                            new_rule[j] = multi_entry[i] if j == idx else rule[j]
                        logger.info("Create event with cron_tab: %s", new_rule)
                        # Create and record the reminder
                        self.reminders[(reminder_name + str(i))] = Reminder(
                            client=self.client,
                            reminder_text=reminder_text,
                            cron_tab=new_rule,
                        )
                else:
                    logger.warn("cront_tab with more than one field with multiple entries is not implemented yet.")
            else:
                start_time = event["start_time"]
                logger.info("start_time: %s", start_time)

                self.reminders[reminder_name] = Reminder(
                    client=self.client,
                    reminder_text=reminder_text,
                    start_time=start_time,
                )


    def _get_cron_tab_from_rrule(self, rrule, start_time):
        # convert the start_time and rrule from an ics calendar to cron_tab format
        # !!!!WARN there is no check atm if the recurring event started already or ended already.
        cron_tab_build = ["", "", "", "", ""]
        logger.debug("rrule.get('FREQ') %s", rrule.get("FREQ")[0])
        if rrule.get("FREQ")[0] == "MINUTELY":
            for i in range(0,4):
                cron_tab_build[i] = "*"
        elif rrule.get("FREQ")[0] == "HOURLY":
            for i in range(1,4):
                cron_tab_build[i] = "*"
            cron_tab_build[0] = str(start_time.minute)
        elif rrule.get("FREQ")[0] == "DAILY":
            for i in range(2,4):
                cron_tab_build[i] = "*"
            cron_tab_build[0] = str(start_time.minute)
            cron_tab_build[1] = str(start_time.hour)
        elif rrule.get("FREQ")[0] == "WEEKLY":
            for i in range(3,4):
                cron_tab_build[i] = "*"
            cron_tab_build[0] = str(start_time.minute)
            cron_tab_build[1] = str(start_time.hour)
            if rrule.get("BYDAY"):
                cron_tab_build[2] = self._get_recur_by_day(rrule.get("BYDAY")[0])
            else:
                cron_tab_build[2] = str(start_time.day)
            cron_tab_build[3] = "*"
            cron_tab_build[4] = "*"
        elif rrule.get("FREQ")[0] == "MONTHLY":
            for i in range(4,5):
                cron_tab_build[i] = "*"
            cron_tab_build[0] = str(start_time.minute)
            cron_tab_build[1] = str(start_time.hour)
            cron_tab_build[3] = "*"
            if rrule.get("BYDAY"):
                cron_tab_build[4] = "*"
                cron_tab_build[2] = self._get_recur_by_day(rrule.get("BYDAY")[0])
            else:
                cron_tab_build[2] = str(start_time.day)
                cron_tab_build[4] = "*"
        elif rrule.get("FREQ")[0] == "YEARLY":
            cron_tab_build[0] = str(start_time.minute)
            cron_tab_build[1] = str(start_time.hour)
            cron_tab_build[2] = str(start_time.day)
            cron_tab_build[3] = str(start_time.month)
            cron_tab_build[4] = "*"

        return cron_tab_build


    def _get_recur_by_day(self, byday):
        day_to_number = {"SU" : "sun", "MO" : "mon", "TU" : "tue", "WE" : "wed", "TH" : "thu", "FR" : "fri", "SA" : "sat"}

        recur = {"1" : "1st", "2" : "2nd", "3": "3rd", "4": "4th", "5": "5th"}
        
        if len(byday) > 2:
            recur_by_day = recur[byday[0]] 
            recur_by_day += " "
            recur_by_day += day_to_number[byday[1:]] 
        else:
            recur_by_day = day_to_number[byday]

        return recur_by_day

