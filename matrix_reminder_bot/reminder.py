import logging
from datetime import datetime, timedelta
from typing import Dict, Optional, Tuple

import pytz
import icalendar
from apscheduler.job import Job
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.date import DateTrigger
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.util import timedelta_seconds
from nio import AsyncClient

from matrix_reminder_bot.config import CONFIG
from matrix_reminder_bot.functions import send_text_to_room

logger = logging.getLogger(__name__)

# The object that runs callbacks at a certain time
SCHEDULER = AsyncIOScheduler()



class Reminder(object):
    """An object containing information about a reminder, when it should go off,
    whether it is recurring, etc.

    Args:
        client: The matrix client
        start_time: When the reminder should first go off
        reminder_text: The text to include in the reminder message
        cron_tab: recurrent event
    """

    def __init__(
        self,
        client: AsyncClient,
        reminder_text: str,
        start_time: Optional[datetime] = None,
        cron_tab: Optional[str] = None
    ):
        self.client = client
        self.time = start_time
        self.reminder_text = reminder_text
        self.cron_tab = cron_tab

        # Schedule the reminder

        # Determine how the reminder is triggered
        if self.cron_tab:
            logger.debug("cron_tab %s", self.cron_tab)
            # Set up a cron trigger
            trigger = CronTrigger(minute=self.cron_tab[0], hour=self.cron_tab[1], month=self.cron_tab[3], day=self.cron_tab[2], timezone=CONFIG.timezone)
        else:
            # Use a date trigger (runs only onc
            trigger = DateTrigger(run_date=start_time, timezone=CONFIG.timezone)

        # Note down the job for later manipulation
        self.job = SCHEDULER.add_job(self._fire, trigger=trigger)

        self.alarm_job = None

    async def _fire(self):
        """Called when a reminder fires"""
        logger.debug("Reminder in room %s fired: %s", CONFIG.room_id, self.reminder_text)

        # Build the reminder message
        message = f"{self.reminder_text}"

        # Send the message to the room
        await send_text_to_room(self.client, message, notice=False)

        # If this was a one-time reminder, cancel and remove from the reminders dict
        if not self.cron_tab:
            # We set cancel_alarm to False here else the associated alarms wouldn't even
            # fire
            self.cancel()

    def cancel(self):
        """Cancels a reminder and all recurring instances

        Args:
            cancel_alarm: Whether to also cancel alarms of this reminder
        """
        logger.debug(
            "Cancelling reminder in room %s: %s", CONFIG.room_id, self.reminder_text
        )

        # Remove from the in-memory reminder and alarm dicts
        REMINDERS.pop((self.reminder_text.upper()), None)

        # Delete any ongoing jobs
        if self.job and SCHEDULER.get_job(self.job.id):
            self.job.remove()





# Global dictionaries
#
# Both feature (room_id, reminder_text) tuples as keys
#
# reminder_text should be accessed and stored as uppercase in order to
# allow for case-insensitive matching when carrying out user actions
REMINDERS: Dict[Tuple[str], Reminder] = {}
