# Fuck Reminder Bot



## Getting started

Create venv with: `python -m venv env`
Start venv with: `source env/bin/activate`
Install requirments: `pip install -r requirements.txt`

Create `config.yaml` file from `sample.config.yaml`.
Add the credentials and information for the matrix room and account.
Add information where to import the events from. There are tw options

1. Import events from ics calendar file. Add path to config.yaml
2. Add events to config.yaml file in the section reminders

Start reminder bot with `python matrix_reminder_bot/main.py`


### Start with docker

build image: `docker build . -t reminder-bot`
run image: `docker run reminder-bot`
